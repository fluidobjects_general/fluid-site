<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="BootstrapFormHelpers/js/bootstrap-formhelpers-phone.js"></script>
<title>Fluid Objects</title>
<link href="media/css/style.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon">
<link rel="stylesheet" href="media/css/gallery.theme.css">
<link rel="stylesheet" href="media/css/gallery.min.css">
<!--scroll suave-->
<script type="text/javascript">
jQuery(document).ready(function($) {
   $.easing.easeInOutExpo = function (x, t, b, c, d) {
      if (t==0) return b;
      if (t==d) return b+c;
      if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
      return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
   }

   $(".scroll").click(function(event){
      event.preventDefault();
      $('html,body').animate({
         scrollTop:$(this.hash).offset().top
      }, {
         duration: 2500,
         easing: 'easeInOutExpo'
      });
   });
});
</script>
<!-- MENU MOBILE-->
<script>
$(document).ready(function(){$(".btn_menu").click(function(){var menu=$(this).attr('data-menu');if(menu=='aberto'){$(".menu_mobile").hide();$(this).attr('data-menu','fechado')}else{$(".menu_mobile").show();$(this).attr('data-menu','aberto')}});$(".menu_mobile").mouseup(function(){return false});$(".btn_menu").mouseup(function(){return false});$(document).mouseup(function(){$(".menu_mobile").hide();$(".btn_menu").attr('data-menu','fechado')})});
</script>
<!-- GOOGLE ANALYTICS-->
<script>

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86345381-1', 'auto');
  ga('send', 'pageview');
</script>

<script>
function sendGAeBook(){
  ga('send', {
    hitType: 'event',
    eventCategory: 'eBook',
    eventAction: 'Download',
    eventLabel: 'download'
  });
}

function sendGAcontact(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Contato',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}
function sendGAcases(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Cases',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}
function sendGAsobre(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Sobre',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}
function sendGAsolucoes(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Soluções',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}
function sendGAblog(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Blog',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}
</script>

<!-- VALIDAR FORMULÁRIOS-->
<script>
function validateContactForm()
{
	var form = document.forms["contact_form"];
	var name = form["nome"].value;
	var phone = form["telefone"].value;
	var subject = form["assunto"].value;
	var email = form["email"].value;
	var message = form["mensagem"].value;
	
	if(name == "")
	{
		alert("Nome deve ser preenchido");
		return false;
	}

	if(phone == " ")
	{
		alert("Telefone deve ser preenchido");
		return false;
	}
	if(subject == "")
	{
		alert("Assunto deve ser preenchido");
		return false;
	}
	if(email == "")
	{
		alert("Email deve ser preenchido");
		return false;
	}
	if(message == "")
	{
		alert("Mensagem deve ser preenchida");
		return false;
	}
	
	//validate email
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
	   alert("Não é um endereço de email válido");
	   return false;
	}
	if(phone.length < 15 || phone.length > 16)
	{
		alert("Telefone inválido");
		return false;
	}
	
	return true;
}	

function mostra_form()
{
	var data = document.getElementById("dados_contato");
	data.style.display = "inline-block";
}

function validateEbookForm()
{
	
	var form = document.forms["ebook_form"];
	var name = form["nome"].value;
	var email = form["email"].value;
	var phone = form["telefone"].value;
	
	if(name == "" && email == ""){
		return false;
	}
	var nome = name.replace(" ", "");
	if(nome == "" || nome == false)
	{
		alert("Nome deve ser preenchido");
		return false;
	}
	if(email == "")
	{
		alert("Email deve ser preenchido");
		return false;
	}
	//validate email
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
	   alert("Não é um endereço de email válido");
	   return false;
	}
	if(phone == " ")
	{
		alert("Telefone deve ser preenchido");
		//alert(name.replace(" ", ""));
		return false;
	}
	if(phone.length < 12 || phone.length > 16)
	{
		alert("Telefone inválido");
		return false;
	}
	grecaptcha.execute();
	return true;
}	



</script>

<!-- GOOGLE RECAPTCHA -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

</head>
<body class="clearfix">
<header>
<div id="logo_container">
<a href="#topo" title="Home" class="scroll"><img src="media/images/logo_fluidobjects.jpg" alt="Logo" style="border:none;" id="logo"></a>
</div>
<div id="menu_desktop">
<div style="width:100%; height:40px; float:right;">
<div style="height:20px; margin:10px 5px; float:right;" class="lang_select"><a href="en/">EN</a></div>
<div style="height:20px; margin:10px 5px; float:right; color:#4488A6; font-weight:bold;">PT</div>
<a href="tel:+555130238823" id="fone_header">+55 51 3023 8823</a>
<div style="height:20px; margin:10px 5px; float:right;">
<div style="height:20px; padding:2px 15px 2px 0px; float:left;">
<a href="https://www.facebook.com/FluidObjects/" target="_blank" class="btn_fb" title="Facebook"></a>
</div>
<div style="height:20px; padding:2px 15px 2px 0px; float:left;">
<a href="https://www.linkedin.com/company/fluid-objects" target="_blank" class="btn_linkedin" title="Linkedin"></a>
</div>
<div style="height:20px; padding:2px 0px; float:left;">
<a href="https://twitter.com/fluidobjects" target="_blank" class="btn_twitter" title="Twitter"></a>
</div>
</div>
</div>
<div style=" width:100%; height:60px; float:left;">
<div class="itens_menu"><a href="#contato" class="scroll" onClick="sendGAcontact()">CONTATO</a></div>
<div class="itens_menu"><a href="https://fluidobjects.blogspot.com.br/" target="_blank" onClick="sendGAblog()">BLOG</a></div>
<div class="itens_menu"><a href="#solucoes" class="scroll"onClick="sendGAsolucoes()">SOLUÇÕES</a></div>
<div class="itens_menu"><a href="#cases" class="scroll" onClick="sendGAcases()">CASES</a></div>
<div class="itens_menu"><a href="#sobre" class="scroll" onClick="sendGAsobre()">SOBRE</a></div>
</div>
</div>
<!-- MENU MOBILE -->
<div id="nav_mobile">
    <span><a class="btn_menu"><img src="media/images/menu.svg" style="width:24px;"></a></span>
    <nav class="menu_mobile">
       <div id="icon-menu_mobile"><img src="media/images/menu.svg" style="width:24px;"></div>
       <div class="itens_menu links_right"><a href="#sobre" class="scroll links_right">SOBRE</a></div>
        <div class="itens_menu links_right"><a href="#cases" class="scroll links_right">CASES</a></div>
        <div class="itens_menu links_right"><a href="#solucoes" class="scroll links_right">SOLUÇÕES</a></div>
        <div class="itens_menu links_right"><a href="https://fluidobjects.blogspot.com.br/" target="_blank" class="links_right">BLOG</a></div>
        <div class="itens_menu links_right"><a href="#contato" class="scroll links_right">CONTATO</a></div>
        <div class="links_left" style="border-top:1px solid #EEE; padding: 0 20px; height: 60px; line-height: 59px; text-align: center; font-weight: 700; color:#4488A6; background:#F9F9F9; cursor:default;">PT</div>
        <div class="itens_menu links_left"><a href="en/" class="links_right">EN</a></div>
    </nav>
    </div>
</header>
<!--Home-->
<section class="container_1" id="topo">
<div>
<div id="header_title">Fluid Objects</div>
<div id="header_subtitle">-  Fazer Mobile é Ser Mobile  -</div>
    <div><a href="#sobre" class="scroll"><button id="btn_outline">SAIBA MAIS SOBRE NÓS</button></a><a href="#contato" class="scroll"><button id="btn_green">ENTRE EM CONTATO</button></a></div>
</div>
</section>
<!--Sobre-->
<section class="container_2" style="height:130%;">
   <div id="sobre"></div>
    <h1>Sobre a Fluid Objects</h1>
    <p class="txt_content">Fundada em 2011, por profissionais com vasta experiência, a Fluid Objects é uma empresa brasileira de tecnologia, pioneira no desenvolvimento de plataformas móveis completas de soluções de negócios. Com compromisso, segurança, acessibilidade e design inovador, a Fluid Objects visa criar soluções digitais que otimizem os serviços da sua empresa a fim de garantir mais rentabilidade para seu negócio.<!--Fundada por profissionais com vasta experiência em tecnologia no Brasil em 2011, foi a primeira empresa a desenvolver uma plataforma completa e móvel de soluções de negócio. 
A Fluid Objects tem como seu principal objetivo desenvolver para seus clientes a melhor solução digital para otimizar os serviços da sua empresa com compromisso, segurança, acessibilidade e design inovador.--></p>
<!-- <div class="form_row">
<div>E-mail</div>
<input type="email" name="email" class="contato_campos" required>    
</div> -->
<form action="contato_pdf.php" method="post" onsubmit="return validateEbookForm();" name="ebook_form">
	<div id="dados_contato" style="width: 65%;padding-left: 35%;padding-top:30px;display:none;">
	Nome <input type="text" name="nome" class="contato_campos" required>
	E-mail <input type="email" name="email" class="contato_campos" required>
	Telefone <input type="text" name="telefone" class="input-medium bfh-phone contato_campos"  data-format=" (dd) ddddd-dddd" required>
    <div class="g-recaptcha"
          data-sitekey="6LdT8B0UAAAAAC2e9yIJftI-eoD0Ax87tHW4lQ4u"
          data-callback="onSubmit"
          data-size="invisible">
    </div>
	</div>
	<div style="padding-left:calc(50% - 125px); margin-top: 1%;">
	<a href="">
		<button onclick='mostra_form()' id="btn_outline"  >BAIXE NOSSO E-BOOK</button>
	</a>
	</div>
</form>

<div id="img_sobre" style="padding-top:5%;">
    <img src="media/images/sobre-devices.png">
</div>
</section>
<!--Cases-->
<section class="container_3">
   <div id="cases"></div>
     <div class="gallery autoplay items-4">
      <div id="item-1" class="control-operator"></div>
      <div id="item-2" class="control-operator"></div>
      <div id="item-3" class="control-operator"></div>
      <div id="item-4" class="control-operator"></div>  

  <figure class="item case1">
    <h1>Cases</h1>
   <div class="txt_content some_height">
   <div class="container_txt_cases">
   <h3>Encontro Natura</h3>
   <p class="subtitle">Aplicativo para iPad</p>
   <p class="txt_cases">
  O aplicativo desenvolvido para a Natura, uma das maiores empresas de cosméticos do Brasil, facilitou o trabalho da equipe de gestão da empresa no monitoramento das consultoras em encontros de consultoria. Reconhecimento facial para controle de demanda, função GPS e catálogo de produtos com uma plataforma para controle de conteúdo foram algumas das funcionalidades criadas para o app.<!--Uma das maiores empresas de cosmético nacional adquiriu um aplicativo mobile onde a equipe de gestão podia monitorar todas as consultoras nos encontros de consultoria, o app tem como funcionalidade reconhecimento fácil para controle de demanda. Incluso também função GPS e catalogo de produtos com uma plataforma para controle de conteúdo.-->
   </p>
   </div>
   <div class="img_cases"><img src="media/images/case-natura.png"></div>
   </div>
  </figure>

  <figure class="item case2">
    <h1>Cases</h1>
    <div class="txt_content some_height">
   <div class="container_txt_cases">
   <h3>Volkswagen</h3>
   <p class="subtitle">Aplicativo para iPad</p>
   <p class="txt_cases">
 Desenvolvimento de um aplicativo mobile para Tablets com a finalidade de oferecer aos vendedores informações automotivas completas na tela de seus aparelhos. Os resultados obtidos pelo uso do software foram o aumento da performance e da concretização de vendas.<!--Foi desenvolvido um aplicativo mobile para Tablets onde os vendedores tivesse em mãos todas as informações automotiva, para aumentar a performance do vendedor, assim fechando a venda mais facilmente.-->
   </p>
   </div>
   <div class="img_cases"><img src="media/images/case-volkswagen.png"></div>
   </div>
  </figure>

  <figure class="item case3">
    <h1>Cases</h1>
    <div class="txt_content some_height">
   <div class="container_txt_cases">
   <h3>Fabrizio Giannone</h3>
   <p class="subtitle">Site responsivo e APP</p>
   <p class="txt_cases">
   Elaboração de plataforma m/e-commerce para a venda de semi-jóias criadas pelo designer italiano Fabrizio Giannone. Com o uso da ferramenta, a empresa passou a contar com distribuição de produtos em todo o Brasil, além das 19 lojas físicas já existentes no país.<!--Foi desenvolvida uma plataforma e-commerce de referência em design de semi-jóias, onde Fabricio descendente de Italianos comercializa suas joias mais bem lapidadas.-->
   </p>
   </div>
   <div class="img_cases"><img src="media/images/case-fg.png"></div>
   </div>
  </figure>
  
  <figure class="item case4">
    <h1>Cases</h1>
    <div class="txt_content some_height">
   <div class="container_txt_cases">
   <h3>Timac Agro</h3>
   <p class="subtitle">Aplicativo para Android</p>
   <p class="txt_cases">
   O aplicativo criado para a Timac Agro visa facilitar o acesso dos vendedores aos produtos e monitorar o desempenho desses por meio do controle de vendas. O resultado, segundo o marketing da empresa, foi um aumento significativo nas vendas e uma aceleração no ramp-up dos vendedores.<!--Empresa consolidada no ramo de Agronegócio, onde foi solicitado um projeto de aplicativo mobile que fornecesse todas as informações conceituais de venda de seus vendedores em campo, segundo Marketing da empresa as vendas aumentaram significativamente e acelerou o ramp-up dos vendedores.-->
   </p>
   </div>
   <div class="img_cases"><img src="media/images/case-timac.png"></div>
   </div>
  </figure>

  <div class="controls">
    <a href="#item-1" class="control-button">•</a>
    <a href="#item-2" class="control-button">•</a>
    <a href="#item-3" class="control-button">•</a>
    <a href="#item-4" class="control-button">•</a>
  </div>
</div>
</section>
<!--Soluções-->
<section class="container_4">
    <div id="solucoes"></div>
    <h1>Soluções</h1>
    <p class="txt_content">Soluções Mobile completas para sua empresa nas mãos de quem realmente entende de tecnologia, para entregar sempre o melhor resultado. Contamos com uma plataforma transparente e acessível para que qualquer usuário trabalhe com agilidade e eficiência.</p>
    <div class="col_center">
    <div class="col_25"><h4>Cloud</h4>
    <img src="media/images/cloud.svg" id="icon_solucoes">
    <p>Ajuste seu espaço conforme a necessidade de sua empresa.</p>
    </div>
    <div class="col_25"><h4>Móvel</h4>
    <img src="media/images/phone.svg" id="icon_solucoes">
    <p>Fácil acesso através de celulares, tablets e computadores.</p>
    </div>
    <div class="col_25"><h4>Offline</h4>
    <img src="media/images/internet.svg" id="icon_solucoes">
    <p>Faça suas alterações em dispositivos no modo on e off-line.</p>
    </div>
    <div class="col_25"><h4>CRM</h4>
    <img src="media/images/crm.svg" id="icon_solucoes">
    <p>Coloque seu cliente como principal foco do seu negócio.</p>
    </div>
    <div class="col_25"><h4>BigData</h4>
    <img src="media/images/database.svg" id="icon_solucoes">
    <p>Importe dados que são significativamente importantes para sua empresa.</p>
    </div>
    <div class="col_25"><h4>Redes Sociais</h4>
    <img src="media/images/share.svg" id="icon_solucoes">
    <p>Integração com as maiores redes sociais dessa geração como Facebook, Twitter, Instagram e Whatsapp.</p>
    </div>
    <div class="col_25"><h4>ChatBot</h4>
    <img src="media/images/chatbot.svg" id="icon_solucoes">
    <p>Melhore o relacionamento com seus clientes com o que tem de mais moderno no mundo digital.</p>
    </div>
    <div class="col_25"><h4>Pagamentos</h4>
    <img src="media/images/pay.svg" id="icon_solucoes">
    <p>Receba pagamentos através do seu site com a segurança que seu cliente precisa.</p>
    </div>
    <div class="col_25"><h4>E-mail Marketing</h4>
    <img src="media/images/mail.svg" id="icon_solucoes">
    <p>Faça campanhas de e-mail marketing dentro da plataforma e acompanhe métricas de quem recebe, abre e converte em suas campanhas.</p>
    </div>
    <div class="col_25"><h4>Customizável</h4>
    <img src="media/images/edit.svg" id="icon_solucoes">
    <p>Soluções digitais para qualquer empresa, nosso sistema conta com as melhores ferramentas para você personalizar como quiser.</p>
    </div>
    </div>
</section>
<!--Contato-->
<section class="container_5">
   <div id="contato"></div> 
<div id="container_contato">
<h1>Contato</h1>
<div id="center_contato">
<div id="content_contato">
<div id="inner_contato">
<h6>Entre em contato conosco</h6>
<div class="form_row"></div>
<form action="enviar_contato.php" method="post" onsubmit="return validateContactForm();" name="contact_form">
<div class="form_row">
<div>Nome</div>
<input type="text" name="nome" class="contato_campos" required>    
</div>
<div class="form_row">
<div>E-mail</div>
<input type="email" name="email" class="contato_campos" required>    
</div>
<div class="form_row form_row_fone">
<div>Telefone</div>
<input type="text" name="telefone" class="input-medium bfh-phone contato_campos" data-format=" (dd) ddddd-dddd" required>
<!--
<input type="text" class="input-medium bfh-phone" data-country="BR">
<input type="number" name="telefone" class="contato_campos" required> -->
</div>
<div class="form_row form_row_assunto">
<div>Assunto</div>
<input type="text" name="assunto" class="contato_campos" required>
</div>
<div class="form_row form_row_mensagem">
<div>Mensagem</div>
<textarea name="mensagem" class="contato_mensagem" required></textarea>
</div>
    <a><button onclick="sendGAcontact()" id="btn_contato_form" type="submit">ENVIAR</button></a>
</div>
</form>
<div id="sidebar_contato">
<div id="logo_sidebar"><img src="media/images/logo_fluidobjects2.png"></div>
<div id="infos_sidebar">
+55 51 3023 8823<br>
info@fluidobjects.com
</div>
<div id="enderecos_sidebar">
Rua Felipe Neri, 434 - Sala 403
Cep 90440-150 - Porto Alegre, RS    
</div>
<div style="height:20px; padding-right:20px; float:left;">
<a href="https://www.facebook.com/FluidObjects/" target="_blank" class="btn_fb_sidebar" title="Facebook"></a>
</div>
<div style="height:20px; padding-right:20px; float:left;">
<a href="https://www.linkedin.com/company/fluid-objects" target="_blank" class="btn_linkedin_sidebar" title="Linkedin"></a>
</div>
<div style="height:20px; padding-right:20px; float:left;">
<a href="https://twitter.com/fluidobjects" target="_blank" class="btn_twitter_sidebar" title="Twitter"></a>
</div>
<div style="width:100%; float:left; padding-top:40px;">
    <a href="https://www.google.com.br/maps/place/R.+Felipe+Neri,+434+-+Auxiliadora,+Porto+Alegre+-+RS,+90440-150/@-30.0226908,-51.1917056,18z/data=!4m5!3m4!1s0x951979ce6e4099d3:0x62ef59319ec8dc85!8m2!3d-30.0229463!4d-51.1912496" target="_blank"><button id="btn_comochegar">COMO CHEGAR</button></a>
</div>
</div>
</div> 
</div>   
</div>
<div class="overlay"></div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.476700426992!2d-51.190955236733814!3d-30.02317895734388!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951979ce73261637%3A0xf6cec53fc26ad88!2sR.+Felipe+Neri%2C+434+-+403+-+Auxiliadora%2C+Porto+Alegre+-+RS%2C+90440-150!5e0!3m2!1spt-BR!2sbr!4v1478486178922" width="100%" height="100%" frameborder="0" style="border:0; position:relative; top:0px;" allowfullscreen></iframe>
</section>
<footer>
<div class="copyright">
Fluid Objects &copy; <?php echo date('Y'); ?> - Todos os direitos reservados
</div>
</footer>
</body>
</html>
