/*SCROLL SUAVE*/

jQuery(document).ready(function($) {
   $.easing.easeInOutExpo = function (x, t, b, c, d) {
      if (t==0) return b;
      if (t==d) return b+c;
      if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
      return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
   }

   $(".scroll").click(function(event){
      event.preventDefault();
      $('html,body').animate({
         scrollTop:$(this.hash).offset().top
      }, {
         duration: 2500,
         easing: 'easeInOutExpo'
      });
   });
});


/*MENU MOBILE*/
$(document).ready(function(){$(".btn_menu").click(function(){var menu=$(this).attr('data-menu');if(menu=='aberto'){$(".menu_mobile").hide();$(this).attr('data-menu','fechado')}else{$(".menu_mobile").show();$(this).attr('data-menu','aberto')}});$(".menu_mobile").mouseup(function(){return false});$(".btn_menu").mouseup(function(){return false});$(document).mouseup(function(){$(".menu_mobile").hide();$(".btn_menu").attr('data-menu','fechado')})});


/*GOOGLE ANALYTICS*/

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86345381-1', 'auto');
  ga('send', 'pageview');
  
  
function sendGAeBook(){
  ga('send', {
    hitType: 'event',
    eventCategory: 'eBook',
    eventAction: 'Download',
    eventLabel: 'download'
  });
}

function sendGAcontact(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Contato',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}
function sendGAcases(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Cases',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}
function sendGAsobre(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Sobre',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}
function sendGAsolucoes(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Soluções',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}
function sendGAblog(){
  ga('send', {
	  hitType: 'event',
	  eventCategory: 'Blog',
	  eventAction: 'Click',
	  eventLabel: 'click'
  })	
}




/*VALIDAR FORMULÁRIO DE CONTATO*/
function validateContactForm()
{
	var form = document.forms["contact_form"];
	var name = form["nome"].value;
	var phone = form["telefone"].value;
	var empresa = form["empresa"].value;
	var email = form["email"].value;
	var message = form["mensagem"].value;
	var cargo = form["cargo"].value;
	
	if(name.trim() == "")
	{
		alert("NOME deve ser preenchido");
		return false;
	}

	if(phone == " ")
	{
		alert("TELEFONE deve ser preenchido");
		return false;
	}
	if(empresa.trim() == "")
	{
		alert("EMPRESA deve ser preenchido");
		return false;
	}
	if(cargo.trim() == "")
	{
		alert("CARGO deve ser preenchido");
		return false;
	}
	if(email.trim() == "")
	{
		alert("EMAIL deve ser preenchido");
		return false;
	}
	if(mensagem.trim() == "")
	{
		alert("MENSAGEM deve ser preenchida");
		return false;
	}
	
	//validate email
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
	   alert("Não é um endereço de email válido");
	   return false;
	}
	if(phone.length < 15 || phone.length > 16)
	{
		alert("Telefone inválido");
		return false;
	}
	return true;
}	


/*MOSTRA FORMULÁRIO DE EBOOK*/
// function mostra_form()
// {
// 	var data = document.getElementById("capas_ebooks");
// 	var fields = document.getElementById("dados_contato");
// 	var button = document.getElementById("button_ebook");
// 	if(fields.style.display == "none")
// 	{
// 		data.style.display = "inline-block";
// 		button.style.display = "none";
// 	}
// 	grecaptcha.execute()

// }

/*MOSTRA CAPAS DOS EBOOKS*/
function mostra_capas()
{
	var data = document.getElementById("capas_ebooks");
	var button = document.getElementById("button_ebook");
	if(data.style.display == "none")
	{
		button.style.display = "none";
		data.style.display = "inline-block";
	}
}

/*SETA A ESCOLHA DO EBOOK*/
function ebook_choice(number)
{
	//var data = document.getElementById("ebook_field");
	//data.value = number;
	var ebooks = document.getElementById("capas_ebooks");
	ebooks.style.display = "none";
	var button = document.getElementById("button_ebook");
	//var form = document.getElementById("dados_contato");
	//form.style.display = "inline-block";

	window.open("form_ebook.php?e="+number, "ebook_window", "height=700, width=1000");
	
	button.style.display = "inline-block";
}


/*VALIDA O FORMULÁRIO PRA BAIXAR EBOOK*/
function validateEbookForm()
{
	
	var form = document.forms["ebook_form"];
	var name = form["nome"].value;
	var email = form["email"].value;
	var phone = form["telefone"].value;

	
	if(name == "" && email == ""){
		return false;
	}
	var nome = name.replace(" ", "");

	if(nome == "" || nome == false)
	{
		alert("Nome deve ser preenchido");
		return false;
	}
	if(email == "")
	{
		alert("Email deve ser preenchido");
		return false;
	}
	//validate email
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
	   alert("Não é um endereço de email válido");
	   return false;
	}
	
	if((phone != " ") && (phone.length < 12 || phone.length > 16))
	{
		alert("Telefone inválido");
		return false;
	}

		return true;

}	


/*HOTJAR TRACKING -----> DEPRECATED */
/*
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:490544,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');*/

/*HEATMAP*/
(function(h,e,a,t,m,p) {
m=e.createElement(a);m.async=!0;m.src=t;
p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
})(window,document,'script','https://u.heatmap.it/log.js');


/*FUNÇÕES PARA IMAGENS DO EBOOK*/
function bigImg(x) {
    x.style.height = "30%";
    x.style.width = "30%";
}

function normalImg(x) {
    x.style.height = "20%";
    x.style.width = "20%";
}

