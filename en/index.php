<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="../BootstrapFormHelpers/js/bootstrap-formhelpers-phone.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript" src="javascript.js"></script>

<!--GOOGLE ADSENSE-->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5418770616606100",
    enable_page_level_ads: true
  });
</script>


<title>Fluid Objects</title>
<link href="../media/css/style.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon">
<link rel="stylesheet" href="../media/css/gallery.theme.css">
<link rel="stylesheet" href="../media/css/gallery.min.css">

</head>


<body class="clearfix">
<header>
<div id="logo_container">
<a href="#topo" title="Home" class="scroll"><img src="../media/images/logo_fluidobjects.jpg" alt="Logo" style="border:none;" id="logo"></a>
</div>
<div id="menu_desktop">
<div style="width:100%; height:40px; float:right;">
<div style="height:20px; margin:10px 5px; float:right; color:#4488A6; font-weight:bold;">EN</div>
    <div style="height:20px; margin:10px 5px; float:right;" class="lang_select"><a href="../">PT</a></div>
<a href="tel:+555130238823" id="fone_header">+55 51 3023 8823</a>
<div style="height:20px; margin:10px 5px; float:right;">
<div style="height:20px; padding:2px 15px 2px 0px; float:left;">
<a href="https://www.facebook.com/FluidObjects/" target="_blank" class="btn_fb" title="Facebook"></a>
</div>
<div style="height:20px; padding:2px 15px 2px 0px; float:left;">
<a href="https://www.linkedin.com/company/fluid-objects" target="_blank" class="btn_linkedin" title="Linkedin"></a>
</div>
<div style="height:20px; padding:2px 0px; float:left;">
<a href="https://twitter.com/fluidobjects" target="_blank" class="btn_twitter" title="Twitter"></a>
</div>
</div>
</div>
<div style=" width:100%; height:60px; float:left;">
<div class="itens_menu"><a href="#contato" class="scroll" onClick="sendGAcontact()">CONTACT</a></div>
<div class="itens_menu"><a href="https://fluidobjects.blogspot.com.br/" target="_blank" onClick="sendGAblog()">BLOG</a></div>
<div class="itens_menu"><a href="#solucoes" class="scroll" onClick="sendGAsolucoes()">SOLUTIONS</a></div>
<div class="itens_menu"><a href="#cases" class="scroll" onClick="sendGAcases()">CASES</a></div>
<div class="itens_menu"><a href="#sobre" class="scroll" onClick="sendGAsobre()">ABOUT</a></div>
</div>
</div>
<!-- MENU MOBILE -->
<div id="nav_mobile">
    <span><a class="btn_menu"><img src="../media/images/menu.svg" style="width:24px;"></a></span>
    <nav class="menu_mobile">
       <div id="icon-menu_mobile"><img src="../media/images/menu.svg" style="width:24px;"></div>
       <div class="itens_menu links_right"><a href="#sobre" class="scroll links_right">ABOUT</a></div>
        <div class="itens_menu links_right"><a href="#cases" class="scroll links_right">CASES</a></div>
        <div class="itens_menu links_right"><a href="#solucoes" class="scroll links_right">SOLUTIONS</a></div>
        <div class="itens_menu links_right"><a href="https://fluidobjects.blogspot.com.br/" target="_blank" class="links_right">BLOG</a></div>
        <div class="itens_menu links_right"><a href="#contato" class="scroll links_right">CONTACT</a></div>
        <div class="links_left" style="border-top:1px solid #EEE; padding: 0 20px; height: 60px; line-height: 59px; text-align: center; font-weight: 700; color:#4488A6; background:#F9F9F9; cursor:default;">PT</div>
        <div class="itens_menu links_left"><a href="en/" class="links_right">EN</a></div>
    </nav>
    </div>
</header>
<!--Home-->
<section class="container_1" id="topo">
<div>
<div id="header_title">Fluid Objects</div>
<div id="header_subtitle">-  Make Mobile is to be Mobile  -</div>
    <div><a href="#sobre" class="scroll"><button id="btn_outline">LEARN MORE ABOUT US</button></a><a href="#contato" class="scroll"><button id="btn_green">CONTACT US</button></a></div>
</div>
</section>
<!--Sobre-->
<section class="container_2" style="height:130%;">
   <div id="sobre"></div>
    <h1>About Fluid Objects</h1>
    <p class="txt_content">Founded in Brazil by professionals with vast experience in technology in 2011, Fluid Objects was the first company to develop a complete and mobile platform of business solutions. The company has as its main goal to develop the best digital solution to optimize your company's services for its customers with commitment, security, accessibility and innovative design.
<!--Fundada por profissionais com vasta experiência em tecnologia no Brasil em 2011, foi a primeira empresa a desenvolver uma plataforma completa e móvel de soluções de negócio. 
A Fluid Objects tem como seu principal objetivo desenvolver para seus clientes a melhor solução digital para otimizar os serviços da sua empresa com compromisso, segurança, acessibilidade e design inovador.--></p>
<!-- <div class="form_row">
<div>E-mail</div>
<input type="email" name="email" class="contato_campos" required>    
</div> -->



<!-- <form action="contato_pdf.php" method="post" onsubmit="return validateEbookForm();" name="ebook_form">
	<div id="capas_ebooks" style="padding-left:40%; padding-top:5%; display:none;">
		<a>
			<img onmouseover="bigImg(this)" onmouseout="normalImg(this)" src="../e1-cover.png" onclick="ebook_choice(1)" style="width:20%;height:20%;padding-right:5%;">
		</a>
		<a>
			<img onmouseover="bigImg(this)" onmouseout="normalImg(this)" src="../e2-cover.png" onclick="ebook_choice(2)" style="width:20%;height:20%;padding-left:5%;">
		</a>
	</div>
	<div id="dados_contato" style="width: 65%;padding-left: 35%;padding-top:30px;display:none;">
		Name <div class="required">*</div>	<input type="text" name="nome" class="contato_campos" required>
		E-mail <div class="required">*</div>	<input type="email" name="email" class="contato_campos" required>
		Phone <input type="text" name="telefone" class="input-medium bfh-phone contato_campos"  data-format=" (dd) ddddd-dddd">
		Company <input type="text" name="empresa" class="contato_campos">
		Role <input type="text" name="cargo" class="contato_campos">
		<input type="text" id="ebook_field" name="ebook" class="contato_campos" style="display:none;" required>
		
    	<div class="g-recaptcha" data-sitekey="6LdT8B0UAAAAAC2e9yIJftI-eoD0Ax87tHW4lQ4u"></div>
	</div>
	<div id="button_ebook" style="padding-left:calc(50% - 125px); margin-top: 1%;">
		<a href="">
			<button onclick='mostra_form()' id="btn_outline">DOWNLOAD OUR E-BOOKS</button>
		</a>
	</div>
</form> -->

<div id="capas_ebooks" style="padding-left:40%; padding-top:5%; display:none;">
    <a>
      <img onmouseover="bigImg(this)" onmouseout="normalImg(this)" src="../e1-cover.png" onclick="ebook_choice(1)" style="width:20%;height:20%;padding-right:5%;">
    </a>
    <a>
      <img onmouseover="bigImg(this)" onmouseout="normalImg(this)" src="../e2-cover.png" onclick="ebook_choice(2)" style="width:20%;height:20%;padding-left:5%;">
    </a>
</div>

<div id="button_ebook" style="padding-left:calc(50% - 125px); margin-top: 1%;">
      <button onclick='mostra_capas();' id="btn_outline">DOWNLOAD OUR E-BOOKS</button>
</div>



<div id="img_sobre" style="padding-top:5%;">
    <img src="../media/images/sobre-devices.png">
</div>
</section>
<!--Cases-->
<section class="container_3">
   <div id="cases"></div>
     <div class="gallery autoplay items-4">
      <div id="item-1" class="control-operator"></div>
      <div id="item-2" class="control-operator"></div>
      <div id="item-3" class="control-operator"></div>
      <div id="item-4" class="control-operator"></div>  

  <figure class="item case1">
    <h1>Cases</h1>
   <div class="txt_content some_height">
   <div class="container_txt_cases">
   <h3>Encontro Natura</h3>
   <p class="subtitle">iPad App</p>
   <p class="txt_cases">
	   The app developed for Natura, one of the largest national cosmetics companies in Brazil, has made the job of monitor the consultants at consulting meetings easier. Facial recognition for demand control, GPS function and product catalog with a platform for content control where some of the functionalities built for the app.
	   <!--Uma das maiores empresas de cosmético nacional adquiriu um aplicativo mobile onde a equipe de gestão podia monitorar todas as consultoras nos encontros de consultoria, o app tem como funcionalidade reconhecimento fácil para controle de demanda. Incluso também função GPS e catalogo de produtos com uma plataforma para controle de conteúdo.-->
   </p>
   </div>
   <div class="img_cases"><img src="../media/images/case-natura.png"></div>
   </div>
  </figure>

  <figure class="item case2">
    <h1>Cases</h1>
    <div class="txt_content some_height">
   <div class="container_txt_cases">
   <h3>Volkswagen</h3>
   <p class="subtitle">iPad app</p>
   <p class="txt_cases">
	A mobile app for Tablets with the purpose of offering complete automotive informations in the sellers's devices. The obtained results by using the software where the increase of performance and sales realization.   
<!--Foi desenvolvido um aplicativo mobile para Tablets onde os vendedores tivesse em mãos todas as informações automotiva, para aumentar a performance do vendedor, assim fechando a venda mais facilmente.-->
   </p>
   </div>
   <div class="img_cases"><img src="../media/images/case-volkswagen.png"></div>
   </div>
  </figure>

  <figure class="item case3">
    <h1>Cases</h1>
    <div class="txt_content some_height">
   <div class="container_txt_cases">
   <h3>Fabrizio Giannone</h3>
   <p class="subtitle">Responsive website and app</p>
   <p class="txt_cases">
	   A reference e-commerce platform has been developed in semi-jewelery design, where Fabricio, descended from Italians, sells his most well-cut jewels.
	   
	   Design of a platform m/e-commerce for the sale of italian designer Fabrizio Giannone's semi-jewelery. With the tool, the company inaugurated a product distribution for the whole country, besides it's 19 already existent physical stores.
   <!--Foi desenvolvida uma plataforma e-commerce de referência em design de semi-jóias, onde Fabricio descendente de Italianos comercializa suas joias mais bem lapidadas.-->
   </p>
   </div>
   <div class="img_cases"><img src="../media/images/case-fg.png"></div>
   </div>
  </figure>
  
  <figure class="item case4">
    <h1>Cases</h1>
    <div class="txt_content some_height">
   <div class="container_txt_cases">
   <h3>Timac Agro</h3>
   <p class="subtitle">Android app</p>
   <p class="txt_cases">
	The application built for Timac Agro aims to ease the sellers' access to products and monitor their performance through sales control. The result, according to the company marketing, was a significative increase in sales and an acceleration in the sellers ramp-up.
 <!--Empresa consolidada no ramo de Agronegócio, onde foi solicitado um projeto de aplicativo mobile que fornecesse todas as informações conceituais de venda de seus vendedores em campo, segundo Marketing da empresa as vendas aumentaram significativamente e acelerou o ramp-up dos vendedores.-->
   </p>
   </div>
   <div class="img_cases"><img src="../media/images/case-timac.png"></div>
   </div>
  </figure>

  <div class="controls">
    <a href="#item-1" class="control-button">•</a>
    <a href="#item-2" class="control-button">•</a>
    <a href="#item-3" class="control-button">•</a>
    <a href="#item-4" class="control-button">•</a>
  </div>
</div>
</section>
<!--Soluções-->
<section class="container_4">
    <div id="solucoes"></div>
    <h1>Solutions</h1>
    <p class="txt_content">Complete Mobile solutions for your company in the hands of who really understands technology, to always deliver the best results. We count with a transparent and accessible platform so that any user can do an efficient and agile work.
	</p>
    <div class="col_center">
    <div class="col_25"><h4>Cloud</h4>
    <img src="../media/images/cloud.svg" id="icon_solucoes">
    <p>Set your space according to your company needs.</p>
    </div>
    <div class="col_25"><h4>Mobile</h4>
    <img src="../media/images/phone.svg" id="icon_solucoes">
    <p>Easy access through smartphones, tablets and computers.</p>
    </div>
    <div class="col_25"><h4>Offline</h4>
    <img src="../media/images/internet.svg" id="icon_solucoes">
    <p>Do your modifications in your devices on and offline.</p>
    </div>
    <div class="col_25"><h4>CRM</h4>
    <img src="../media/images/crm.svg" id="icon_solucoes">
    <p>Focus your business mainly on your client.</p>
    </div>
    <div class="col_25"><h4>BigData</h4>
    <img src="../media/images/database.svg" id="icon_solucoes">
    <p>Import data that are of the utmost importance for your company.</p>
    </div>
    <div class="col_25"><h4>Social Networks</h4>
    <img src="../media/images/share.svg" id="icon_solucoes">
    <p>Integration with the most important social networks of this generation, as Facebook, Twitter, Instagram and Whatsapp.</p>
    </div>
    <div class="col_25"><h4>ChatBot</h4>
    <img src="../media/images/chatbot.svg" id="icon_solucoes">
    <p>Use modern digital tools to improve your relationship with the client.</p>
    </div>
    <div class="col_25"><h4>Payments</h4>
    <img src="../media/images/pay.svg" id="icon_solucoes">
    <p>Receive payments through your website with the security your client needs.</p>
    </div>
    <div class="col_25"><h4>E-mail Marketing</h4>
    <img src="../media/images/mail.svg" id="icon_solucoes">
    <p>Do marketing campaigns via e-mail within the platform and keep track of who receives, opens and converts in your campaigns.</p>
    </div>
    <div class="col_25"><h4>Customizable</h4>
    <img src="../media/images/edit.svg" id="icon_solucoes">
    <p>Digital solutions for any company, our system has the best tools for you to customize as you want.</p>
    </div>
    </div>
</section>
<!--Contato-->
<section class="container_5">
   <div id="contato"></div> 
<div id="container_contato">
<h1>Contact</h1>
<div id="center_contato">
<div id="content_contato">
<div id="inner_contato">
<h6>Hello! Thank you for getting in touch with us!<br><br>Please, fill the form below so that we can better understand your needs and help you.</h6>
<div class="form_row"></div>
<form action="enviar_contato.php" method="post" onsubmit="return validateContactForm();" name="contact_form">
<div class="form_row form_row_name">
	<div>Name <div class="required">*</div></div>
		<input type="text" name="nome" class="contato_campos" required>    
</div>

<div class="form_row form_row_fone">
	<div>Phone <div class="required">*</div></div>
		<input type="text" name="telefone" class="input-medium bfh-phone contato_campos" data-format=" (dd) ddddd-dddd" required>
</div>

<div class="form_row">
	<div>E-mail <div class="required">*</div></div>
		<input type="email" name="email" class="contato_campos" required>    
</div>

<div class="form_row form_row_empresa">
	<div>Company <div class="required">*</div></div>
		<input type="text" name="empresa" class="contato_campos" required>
</div>

<div class="form_row form_row_cargo">
	<div>Role <div class="required">*</div></div>
		<input type="text" name="cargo" class="contato_campos" required>
</div>


<div class="form_row form_row_mensagem">
<div>Tell us more about what your company needs and the goals you seek.</div>
<textarea name="mensagem" class="contato_mensagem" required></textarea>
<div class="g-recaptcha" data-sitekey="6LdT8B0UAAAAAC2e9yIJftI-eoD0Ax87tHW4lQ4u"></div>
</div>


<a><button onclick="sendGAcontact()" id="btn_contato_form" type="submit">SEND</button></a>
</div>
</form>
<div id="sidebar_contato">
<div id="logo_sidebar"><img src="../media/images/logo_fluidobjects2.png"></div>
<div id="infos_sidebar">
+55 51 3023 8823<br>
info@fluidobjects.com
</div>
<div id="enderecos_sidebar">
Rua Felipe Neri, 434 - Sala 403
Cep 90440-150 - Porto Alegre, RS    
</div>
<div style="height:20px; padding-right:20px; float:left;">
<a href="https://www.facebook.com/FluidObjects/" target="_blank" class="btn_fb_sidebar" title="Facebook"></a>
</div>
<div style="height:20px; padding-right:20px; float:left;">
<a href="https://www.linkedin.com/company/fluid-objects" target="_blank" class="btn_linkedin_sidebar" title="Linkedin"></a>
</div>
<div style="height:20px; padding-right:20px; float:left;">
<a href="https://twitter.com/fluidobjects" target="_blank" class="btn_twitter_sidebar" title="Twitter"></a>
</div>
<div style="width:100%; float:left; padding-top:40px;">
    <a href="https://www.google.com.br/maps/place/R.+Felipe+Neri,+434+-+Auxiliadora,+Porto+Alegre+-+RS,+90440-150/@-30.0226908,-51.1917056,18z/data=!4m5!3m4!1s0x951979ce6e4099d3:0x62ef59319ec8dc85!8m2!3d-30.0229463!4d-51.1912496" target="_blank"><button id="btn_comochegar">HOW TO GET HERE</button></a>
</div>
</div>
</div> 
</div>   
</div>
<div class="overlay"></div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.476700426992!2d-51.190955236733814!3d-30.02317895734388!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951979ce73261637%3A0xf6cec53fc26ad88!2sR.+Felipe+Neri%2C+434+-+403+-+Auxiliadora%2C+Porto+Alegre+-+RS%2C+90440-150!5e0!3m2!1spt-BR!2sbr!4v1478486178922" width="100%" height="100%" frameborder="0" style="border:0; position:relative; top:0px;" allowfullscreen></iframe>
</section>
<footer>
<div class="copyright">
Fluid Objects &copy; <?php echo date('Y'); ?> - Todos os direitos reservados
</div>
</footer>
</body>
</html>
