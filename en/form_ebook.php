<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- JAVASCRIPT -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="../BootstrapFormHelpers/js/bootstrap-formhelpers-phone.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript" src="javascript.js"></script>

<!-- CSS AND FONTS -->
<link href="../media/css/style.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="favicon.ico" type="image/vnd.microsoft.icon">
<link rel="stylesheet" href="../media/css/gallery.theme.css">
<link rel="stylesheet" href="../media/css/gallery.min.css">


	<title>E-Book Form</title>
</head>
<body class="container_2">

	<?php 
		$ebook_number = $_GET['e'];
	?>

	<form action="contato_pdf.php" method="post" onsubmit="return validateEbookForm();" name="ebook_form">

	<div id="dados_contato" style="width: 65%;padding-left: 35%;padding-top:30px;">
		Fill the form to download our e-book for free.<br><br>
		Name <div class="required">*</div>	<input type="text" name="nome" class="contato_campos" required>
		E-mail <div class="required">*</div>	<input type="email" name="email" class="contato_campos" required>
		Phone <input type="text" name="telefone" class="input-medium bfh-phone contato_campos"  data-format=" (dd) ddddd-dddd">
		Company <input type="text" name="empresa" class="contato_campos">
		Role <input type="text" name="cargo" class="contato_campos">
		<input type="text" value="<?php echo $ebook_number;?>" id="ebook_field" name="ebook" class="contato_campos" style="display:none;" required>

    	<div class="g-recaptcha" data-sitekey="6LdT8B0UAAAAAC2e9yIJftI-eoD0Ax87tHW4lQ4u"></div>
		
		
	</div>

	<div style="padding-left:calc(50% - 125px); margin-top: 1%;">
		<button type="submit" id="btn_outline">SEND</button>
	</div>

	</form>
</body>
</html>