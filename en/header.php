<header>
<div style="margin:0 15%;">
<div style="width:20%; height:100px; float:left;">
 <div style="width:60px; height:80px; margin:10px 0;"><a href="#topo" title="Home" class="scroll"><img src="../media/images/logo_fluidobjects.jpg" alt="Logo" style="border:none;"></a></div>
</div>
<div style="width:80%; height:100px; float:right;">
<div style="width:100%; height:40px; float:right;">
<div style="height:20px; margin:10px 5px; float:right; color:#4488A6; font-weight:bold;">EN</div>
<div style="height:20px; margin:10px 5px; float:right;" class="lang_select"><a href="../">PT</a></div>
<div style="float:right; height:20px; margin:10px; padding:0 15px; border-right:1px solid #EEE; border-left:1px solid #EEE; font-weight:700; text-align:center;">
+55 51 3073 4861
</div>
<div style="height:20px; margin:10px 5px; float:right;">
<div style="height:20px; padding:2px 15px 2px 0px; float:left;">
<a href="https://www.facebook.com/FluidObjects/" target="_blank" class="btn_fb" title="Facebook"></a>
</div>
<div style="height:20px; padding:2px 15px 2px 0px; float:left;">
<a href="https://www.linkedin.com/company/fluid-objects" target="_blank" class="btn_linkedin" title="Linkedin"></a>
</div>
<div style="height:20px; padding:2px 0px; float:left;">
<a href="https://twitter.com/fluidobjects" target="_blank" class="btn_twitter" title="Twitter"></a>
</div>
</div>
</div>
<div style=" width:100%; height:60px; float:left;">
<div class="itens_menu"><a href="#contact" class="scroll">CONTACT</a></div>
<div class="itens_menu"><a href="http://blog.fluidobjects.com.br/" target="_blank">BLOG</a></div>
<div class="itens_menu"><a href="#development" class="scroll">DEVELOPMENT</a></div>
<div class="itens_menu"><a href="#solutions" class="scroll">SOLUTIONS</a></div>
<div class="itens_menu"><a href="#about" class="scroll">ABOUT</a></div>
</div>
</div>
</div>
</header>